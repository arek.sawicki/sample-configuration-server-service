package com.acme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/***
 * Some legacy clients (like ibm/mq/connectionfactory/ccdt-url) need binary configuration files.
 * By default spring-cloud-config-server serves all files as plain-text/utf-8, as a result binaries are corrupted.
 *
 * Adding header "Accept: application/octet-stream" to requests tells to serve binary content (spring-mvc)
 * Instrumentation is made to specific file extensions only (ccdt, tab).
 * i.e: http://CONFIG-SERVER/foo/default/master/file.ccdt
 *
 */
@Component
public class BinaryFileExtensionFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(BinaryFileExtensionFilter.class);

    private static final List<String> BINARY_FILE_EXTENSIONS = Arrays.asList(
            ".ccdt",
            ".tab"
    );

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

        if (isBinaryFileExtensionRequested(httpServletRequest.getRequestURI())) {
            logger.info("Requested binary file {}", httpServletRequest.getRequestURI());
            AcceptOctetStreamHeaderRequest acceptHeaderRequest = new AcceptOctetStreamHeaderRequest(httpServletRequest);
            filterChain.doFilter(acceptHeaderRequest, servletResponse);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

    private boolean isBinaryFileExtensionRequested(String requestURI) {
        if (requestURI == null) {
            return false;
        }
        return BINARY_FILE_EXTENSIONS.stream()
                .anyMatch(extension -> requestURI.toLowerCase().endsWith(extension));
    }
}
