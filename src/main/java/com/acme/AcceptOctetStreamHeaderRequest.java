package com.acme;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.*;

/***
 * Extend servlet request with headers:
 *
 * accept: application/octet-stream
 * content-type: application/octet-stream
 *
 */
public class AcceptOctetStreamHeaderRequest extends HttpServletRequestWrapper {
    private static final String APPLICATION_OCTET_STREAM = "application/octet-stream";

    AcceptOctetStreamHeaderRequest(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getHeader(String name) {
        if (name.equalsIgnoreCase("accept") || name.equalsIgnoreCase("content-type")) {
            return APPLICATION_OCTET_STREAM;
        } else {
            return super.getHeader(name);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Enumeration getHeaders(String name) {
        if (name.equalsIgnoreCase("accept") || name.equalsIgnoreCase("content-type")) {
            return new StringTokenizer(APPLICATION_OCTET_STREAM);
        } else {
            return super.getHeaders(name);
        }
    }

    @Override
    public String getContentType() {
        return APPLICATION_OCTET_STREAM;
    }
}
