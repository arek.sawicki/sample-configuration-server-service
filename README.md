# Sample spring-cloud-configuration-server with binary files support

By servlet filter instrumentation request is extended with additional headers.

Request header `Accept: 'application/octet-stream'` tells to serve file as binary content.

Binary mode is enabled to file extension whitelist only.